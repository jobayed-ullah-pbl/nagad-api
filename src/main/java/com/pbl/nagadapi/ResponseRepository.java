package com.pbl.nagadapi;

import com.pbl.nagadapi.entity.ResponseAccCheck;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResponseRepository extends JpaRepository<ResponseAccCheck,Long> {
}
